const express = require("express")
const router = express.Router()
const auth = require("../auth")
const CourseController = require("../controllers/course")


//routes

//get all active courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(resultFromGetAllActive => res.send(resultFromGetAllActive))
})

//get all courses
router.get("/all", (req, res) => {
	CourseController.getAll().then(result => res.send(result))
})


//get a single course
router.get("/:courseId", (req, res) => {
	let courseId = req.params.courseId
	CourseController.get({courseId}).then(resultFromGet => res.send(resultFromGet))
})


//create a new course
router.post("/", auth.verify, (req, res) => {
	CourseController.add(req.body).then(resultFromAdd => res.send(resultFromAdd))
})


//update a course
router.put("/", auth.verify, (req, res) => {
	CourseController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})


//archive a course
router.delete("/:courseId", auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.archive({courseId}).then(resultFromArchive => res.send(resultFromArchive))
})

//activate a course
router.delete("/active/:courseId", auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.active({courseId}).then(resultFromActive => res.send(resultFromActive))
})


module.exports = router 