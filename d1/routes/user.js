const express = require("express")
const router = express.Router() //this provide the routes ie. .get(), .post(), etc
//the router was not included in s18, as both the server and router are part of the same file
const auth = require("../auth") //need for login
const UserController = require("../controllers/user")

/*
app.post("/tasks", (req, res) => {
	(A) let newTask = {}
	(B) newTask.save
	return res.send
})

Lines A and B process the request 
If we are to follow the separation of concern, we simply tell the route to call on the UserController functions
*/


router.post("/email-exists", (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})


//Registration
router.post("/", (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(
		resultFromRegister))
})



//Login
//post is used since we send the data to create a logged in session
router.post("/login", (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})



//Get details of a user
//auth.verify ensures that a user is logged in before proceeding to the next part of the code.
//auth.verify is that is called a middleware, a middleware's job is like a gate wherein only those with permission can proceed
router.get("/details", auth.verify, (req, res) => {
	//get the decoded token, the decoded token already has some details from the user
	const user = auth.decode(req.headers.authorization)
	//the result is the data from auth.js (id,email,isAdmin)

	//We use id of the user drom the token to search for additional information of the user.
	UserController.get({userId: user.id}).then(user => res.send(user))
})


//Enroll
//check if the user os logged in, get its id and get the course the student wants to enroll in
router.post("/enroll", auth.verify, (req, res) => {
	let params = {
		userId: auth.decode(req.headers.authorization).id,
		//userId comes from decoding the JWT and getting the id field
		courseId: req.body.courseId
		//courseId comes from the request body
	}

	UserController.enroll(params).then(resultFromEnroll =>  res.send(resultFromEnroll))

})


//google
router.post('/verify-google-id-token', async (req, res) => {

	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))

})


//update user
router.put("/", auth.verify, (req, res) => {	
	console.log(req.body)
	UserController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})






module.exports = router 