//dependencies setup
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")


//database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:ifqQ3IcNQTjCcwZu@learnmongo-9fdce.gcp.mongodb.net/restbooking?retryWrites=true&w=majority', { 
	useNewUrlParser: true, 
	useUnifiedTopology: true 
})


//server setup
const app = express()
const port = 4000
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors()) //This will be used if other sites would use this API


//Add all the routes
app.use("/api/users", userRoutes)
//bt doing this, the server will call all user routes in user.js and will begin with /api/users
//ex. /api/users/enroll, api/users/login
app.use("/api/courses", courseRoutes)
//ex. api/courses/:courseId



//server listening
app.listen(port, () => console.log(`Listening to port ${port}`))

