const User = require("../models/user") //With module.exports the user.js from models can be called as part of the require

//Create course model
const Course = require("../models/course")
const bcrypt = require("bcrypt") //Allows encrypt of strings
//if the module/package required is part of npm, no need to specify the path
//if the module/package is custom made, then path needs to be specified

const auth = require("../auth") //is used for authorization of requests

const sms = require("../sms")

//require the google auth library in expressjs:
const{ OAuth2Client } = require('google-auth-library')
//provide client id
const clientId = '1042993511505-d27s3fdfhe4pfluauvf0cu4dn77g0or6.apps.googleusercontent.com'


//functions

//check if email already exists
module.exports.emailExists = (parameterFromRoute) => {
	//Steps
	//1. Use mongoose's find() using the email
	//2. the then() expects a response or result from the find()
	//3. use the result from find, and check if the length is 0 or not
	//4. The ternary operator then returns either true or false
	return User.find({ email: parameterFromRoute.email }).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
	//params.email would come from the request when we use the controller in the routes
}



//registration
module.exports.register = (params) => {
	let newUser = new User({
		firstname: params.firstname,
		lastname: params.lastname,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
		//hashSync() hashes/encrypt and 10 is salt, it hases 10 times
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
		//if there is an error in saving, returns false meaning the user was not saved,
		//if there are no errors, returns true meaning a new user was created
		/*
			(err) ? false : true is the ternary operator
			if(err){
				return false
			} else {
				return true
			}
		*/
	})
}



//login
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne =>{
		if (resultFromFindOne == null){ //user doesnt exist
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
		//compareSync() is used to compare a nonhashed password to a hashed password, this returns a boolean value (true of false)
		//a good coding practice in naming boolean variables is in form of is<Noun>
		//ex. isLegal, isPasswordMatched, isDone

		//at this point, there is a user and the passwords are compared
		if(isPasswordMatched){
			return{ access : auth.createAccessToken(resultFromFindOne.toObject())}
			//auth is custom module that uses JWT to pass data around
			//createAccessToken is a method in auth that creates a JWT with the user as part of the token
		} else {
			return false
		}
	})
}



//get user profile
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}


//enroll to a class
module.exports.enroll = (params) => {
	//1. get the user
	//2. push the courseId to the user's enrollments array
	//3. save the updated user
	//4. Get the course
	//5. Push the user's is in the course's enrollees array
	//6. save the updated course

	//find the student first then push the course
	return User.findById(params.userId).then(resultFromFindByIdUser => {
		resultFromFindByIdUser.enrollments.push({courseId: params.courseId})

		return resultFromFindByIdUser.save().then((resultFromSaveUser, err) => {
			//find the course first then push the student
			return Course.findById(params.courseId).then(resultFromFindByIdCourse => {
				resultFromFindByIdCourse.enrollees.push({userId: params.userId})

				return resultFromFindByIdCourse.save().then((resultFromSaveCourse, err) => {
					if(err){

						return false

					} else {

						console.log(resultFromFindByIdUser)
						sms.send(resultFromFindByIdUser.mobileNo, `Thank you for enrolling to ${resultFromFindByIdCourse.name}. You have enrolled to this course on: ${ new Date().toLocaleString() }.`)
						return true

					}

				})
			})
		})
	})
}

//similar to your course update
//Update Controller:

module.exports.update = (params) => {

	console.log(params)	

	let updatedUser = {			

		firstname: params.firstname,
		lastname: params.lastname,
		mobileNo: params.mobileNo

	}
	//we take the details from the body and pass into an object which will be used to update the users profile

	//update the user, the first, params.id = _id of the user in the database.
	//second argument is our updatedUser object which will be passed on and update our profile. then if the user has been edited successfully or not, we will send a boolean back to our client.
	return User.findByIdAndUpdate(params.id, updatedUser).then((user,err) => {
		return (err) ? false : true


	})

}


module.exports.verifyGoogleTokenId = async(tokenId) => {

//create a new OAuth2Client from google auth library, to verify and check our token.

const client = new OAuth2Client(clientId)
const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
//audience will check your token side by side if its coming from a recognizable source/

//lets check the data responded by google verify
//lets check if the email used is already in our DB
if(data.payload.email_verified === true){

	//check the DB if the email found in the data.payload sent by google is already registered in our DB
	const user = await User.findOne({ email: data.payload.email }).exec()
	//in mongoose, .exec()  works almost like .then() that it then allow execution of the following statements.
	//we use this if an await keyword was used.	
	//if the user logs a null vlaue, then the email logged into google, hasnt been registered to our DB

	if( user !== null ){
		console.log('A user has been registered')
		if(user.loginType === 'google'){
			//if else statement to check if the user logging in used a google login to register for the first time
			return{ access: auth.createAccessToken(user.toObject())}

		} else {

			return { error: 'login-type-error' }

		}

	} else {
		//check if data.payload is coming in
		console.log(data.payload)

		const newUser = new User({

			firstname: data.payload.given_name,
			lastname: data.payload.family_name,
			email: data.payload.email,
			loginType: 'google'
		})

		return newUser.save().then((user, err) => {

			console.log(user)
			return{ access: auth.createAccessToken(user.toObject())}
			//if a new google user logins for the first time, he is registered to our database and once he's been registered, we'll be returning a token

		})

	}

} else {

	//an error in checking the token
	return { error: 'google-auth-error' }

}



console.log(data)

}

