const Course = require("../models/course")


//functions for course controller

//view all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(resultFromFind => resultFromFind)
}


//view all course
module.exports.getAll = () => {
	return Course.find({}).then(resultFromFind => resultFromFind)
}


//Create/Register a course
module.exports.add = (params) => {
	let newCourse = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})
	return newCourse.save().then((course, err) => {
		return (err) ? false : true
	})
}


//Get a course
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(resultFromFind => resultFromFind)
	//params.courseId is programmer defined
}


//Modify a course
module.exports.update = (params) => {
	let updatedCourse = {
		name: params.name,
		description: params.description,
		price: params.price
	}
	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course, err) => {
		return (err) ? false : true
	})
	/*
	Last time, we did findById, then modify the fields and then save() but with findByIdAndUpdate, we just need to specify 2 things. 1.id, 2.fields we want to update inside an object
	*/
}


//Activate a course
module.exports.active = (params) => {
	let updateActive = {
		isActive: true
	}
	return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
		return (err) ? false : true	
	})
}

//Archive a course - set isActive to false
module.exports.archive = (params) => {
	let updateActive = {
		isActive: false
	}
	return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
		return (err) ? false : true
		//this line returns false if an error was encountered when updating a course
		//this line returns true if no error were encountered
		//the result of archive would be just true or false
	})
}